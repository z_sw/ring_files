#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cx_Freeze import setup, Executable
import os.path
import sys

buildOptions = dict(
        packages = [
            'pathlib',],
        excludes = [],
        include_files = [
            ],
        )

base = None

executables = [
    Executable('ring_files.py', base=base,
        icon='app.ico',
        copyright = "Copyright 2018, Zhang Shaowei<z_sw@hotmail.com>"
        )
]

setup(name='RingFiles',
      version = '0.0.1',
      description = 'Utility remove more files',
      options = dict(build_exe = buildOptions),
      executables = executables)
