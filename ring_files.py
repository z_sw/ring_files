#!/bin/env python
# -- utf-8 ---

import sys, datetime, time, gzip, shutil
from stat import *
from pathlib import Path

def usage():
    print('Usage:')
    print('\tring_files <period>')
    print('Please make config.txt as configuration')
    print('\tAuthor: Zhang Shaowei<z_sw@hotmail.com>')

if __name__ == '__main__':
    if len(sys.argv) == 2:
        while True:
            for c in open('config.txt'):
                l = c.strip()
                if l.startswith('#') or not len(l):
                    continue
                
                pars = l.split()
                if len(pars) < 2:
                    print('Wrong configuration line: ', l)
                elif pars[0] == 'remove' and len(pars) == 4:
                    print('Process: {}'.format(l))
                    rm_files = sorted(
                            [(f, f.stat().st_ctime)
                                for f in Path(pars[1]).glob(pars[2])],
                            key=lambda info: info[1],
                            reverse=True
                            )
                    num = int(pars[3])
                    for i in rm_files[num:]:
                        if i[0].is_file() and i[0].exists():
                            print(' Remove {}'.format(str(i[0])))
                            i[0].unlink()

                elif pars[0] == 'compress' and len(pars) == 2:
                    print('Process: ', l)
                    com_files = sorted(
                            [(f, f.stat().st_ctime)
                                for f in Path(pars[1]).glob('*') if not str(f).endswith('.gz')],
                            key=lambda info: info[1],
                            reverse=True
                            )
                    for i in com_files[1:]: # Skip current recording file
                            print(' Compressing ', str(i[0]))
                            with open(str(i[0]), 'rb') as f_in:
                                with gzip.open(str(i[0]) + '.gz', 'wb') as f_out:
                                    shutil.copyfileobj(f_in, f_out)

                            if Path(str(i[0]) + '.gz').exists() and i[0].exists():
                                i[0].unlink()
                else:
                    print('Wrong configuration line: ', l)

            time.sleep(int(sys.argv[1]))
    else:
        usage()

